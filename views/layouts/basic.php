<?php
use yii\helpers\Html;
use yii\web\UrlManager;
use yii\helpers\Url;
use app\assets\AppAsset;



AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

<meta charset="<?= Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?= Html::csrfMetaTags() ?>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
<title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
</head>

<?php $this->beginBody() ?>
<body>




<?= $content ?>






<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

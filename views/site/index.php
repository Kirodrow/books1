<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
?>


<div class="padding">


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'ajax_form'  ]]);?>
    <?= $form->field($model, 'name')->widget(Typeahead::classname(), [
        'options' => ['placeholder' => 'Filter as you type ...'],
        'pluginOptions' => ['highlight'=>true],
        'dataset' => [
            [
                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                'display' => 'value',
                'prefetch' =>  'site/json',
                'remote' => [
                    'url' => Url::to(['site/list']) . '?q=%QUERY',
                    'wildcard' => '%QUERY'
                ]
            ]
        ]
    ]);?>


    <span><?=Html::submitButton('вывести список книг поиска', ['class' => 'btn btn-primary', 'id'=> 'btn',
            'type' => 'submit','style' => ' width: 300px']);?></span>
    <?php ActiveForm::end() ?>
    <br>
    <span><?= Html::a('Показать список всех книг', Url::to('site/show'), ['class' => 'btn btn-primary all',
            'style' => ' width: 300px'])?></span>



    <span><?= Html::a('Редактировать книги', Url::to('post/index'), ['class' => 'btn btn-primary',
            'style' => ' width: 300px'])?></span>
    <span><?= Html::a('Редактировать рубрики', Url::to('rubric/index'), ['class' => 'btn btn-primary',
            'style' => ' width: 300px'])?></span>


    <table border="1" class="form" >
        <caption>список книг</caption>
        <tr>
            <th class="newclass" colspan="4">Книги</th>

            <th class="newclass1">Рубрика</th>
        </tr>
        <tr ><td>Наименование</td><td>Автор</td><td>Дата публикации</td><td>ISBN</td><td>Наименование рубрики</td></tr>

    </table>

    <br>
</div>
<section class="www"></section>


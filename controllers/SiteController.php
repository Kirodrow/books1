<?php
namespace app\controllers;


//use function Symfony\Component\Debug\Tests\FatalErrorHandler\test_namespaced_function;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use app\models\Book;
use app\models\Rubric;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use yii\helpers\Json;
use yii\db\Query;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = 'basic';
    public $mass = array();

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /* 'access' => [
                 'class' => AccessControl::className(),
                 'only' => ['logout', 'signup'],
                 'rules' => [
                     [
                         'actions' => ['signup'],
                         'allow' => true,
                         'roles' => ['?'],
                     ],
                     [
                         'actions' => ['logout'],
                         'allow' => true,
                         'roles' => ['@'],
                     ],
                 ],
             ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Book();

        return $this->render('index', compact('model'));
    }





    public function actionNew() // показывает книги исходя из запроса
    {

        $val = Yii::$app->request->post('Book')['name'];

        $result = Book::find()->where(['like', 'name', $val] )->asArray()->with('rubric')->all();
        unset($_POST['Book']);
        $res = json_encode($result);
        return $res;
    }


    public function actionShow() //показывает все книги из бд
    {

        $result = Book::find()->asArray()->with('rubric')->all();
        $res = json_encode($result);
        return $res;

    }



    public function actionList ( $q = null ) {
        $query = new Query() ;
        $session = Yii::$app->session;
        $session->set('search', $q);
        $query->select( 'name' )->from( 'book' ) ->where( 'name LIKE "%' . $q . '%"' )
            -> orderBy ( 'name' );
        $command = $query -> createCommand ();
        $data = $command -> queryAll ();
        $out = [];
        foreach ( $data as $d ) {
            $out[] = [ 'value' => $d [ 'name' ]];
        }
        echo Json::encode( $out );
    }

    public function actionJson(){
        $a[] = 'тест';
        return $this->asJson($a);
    }
}




